export default [
  {
    name  : 'profile',
    title : 'Profile',
    url   : '/setting/profiles',
    icon  : 'icon-user',
    access: ['User'],
  },
  {
    name  : 'setting',
    title : 'Setting',
    url   : '/setting',
    icon  : 'icon-menu',
    access: ['Barista', 'Superadmin'],
  },
  {
    name  : 'orders',
    title : 'Orders',
    url   : '/orders',
    icon  : 'icon-basket-loaded',
    access: ['Superadmin', 'User'],
  },
  {
    name  : 'history',
    title : 'History',
    url   : '/history',
    icon  : 'icon-list',
    access: ['User', 'Superadmin'],
  },
  {
    name  : 'queue',
    title : 'Queue',
    url   : '/queue',
    icon  : 'icon-clock',
    access: ['Barista', 'Superadmin'],
  },
  {
    name  : 'completeOrder',
    title : 'Complete Order',
    url   : '/complete/order',
    icon  : 'icon-user-following',
    access: ['Barista', 'Superadmin'],
  },
]
