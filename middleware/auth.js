export default function ({ store, redirect, app, route }) {
  const token = app.$cookies.get('session/token')

  if (!token)
    return redirect('/', route.path !== '/' ? { redirect: route.path } : null)

  store.set('session/token', token)
}
