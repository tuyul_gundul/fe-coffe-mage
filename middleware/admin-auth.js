export default function ({ store,
  redirect,
  route }) {
  const role = store.get('session/user/role')

  if (role !== 'Superadmin')
    return redirect('/login', route.path !== '/' ? { redirect: route.path } : null)
}
