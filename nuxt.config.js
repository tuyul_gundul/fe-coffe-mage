const webpack = require('webpack')
const pkg     = require('./package')

require('dotenv').config()

module.exports = {
  mode: 'universal',
  env : {
    apiUrl : process.env.API_URL,
    apiFile: process.env.API_FILE,
  },
  head: {
    title: 'PRIVY POS',
    meta : [
      { charset: 'utf-8' },
      {
        name   : 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      {
        hid    : 'description',
        name   : 'description',
        content: pkg.description,
      },
    ],
    link: [
      {
        rel : 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico',
      },
      {
        rel : 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Lato:300,400,700',
      },
    ],
  },
  loading: '@/components/Loading.vue',
  css    : ['@/assets/scss/app.scss'],

  plugins: [
    {
      src: '@/plugins/datepicker',
      ssr: false,
    },
    {
      src: '@/plugins/datetimepicker',
      ssr: false,
    },
    {
      src: '@/plugins/overlay',
      ssr: false,
    },
    {
      src: '@/plugins/switch',
      ssr: false,
    },
    {
      src: '@/plugins/notify',
      ssr: false,
    },
    { src: '@/plugins/axios' },
    { src: '@/plugins/menu' },
  ],

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    '@nuxtjs/proxy',
    'nuxt-fontawesome',
    'cookie-universal-nuxt',
    'nuxt-simple-line-icons',
    'vue-sweetalert2/nuxt',
    ['@nuxtjs/moment', { plugin: false }],
    ['bootstrap-vue/nuxt', { css: false }],
  ],
  proxy: {
    '/api-admin': {
      target: process.env.API_ADMIN_URL,
      pathRewrite: { '^/api-admin': '/' },
    },
    onProxyReq(proxyRequest, request, response) {
      const cookies = cookiesParser(request, response)
      const token = cookies.get('session/token')

      proxyRequest.setHeader('Authorization', `Bearer ${token}`)
    },
  },
  axios: {
    browserBaseURL: '/api-admin',
    proxy         : true,
  },
  fontawesome: {
    component: 'fa',
    imports  : [
      {
        set  : '@fortawesome/free-solid-svg-icons',
        icons: ['fas'],
      },
    ],
  },
  build: {
    babel  : { plugins: ['lodash'] },
    loaders: {
      vue: {
        transformAssetUrls: {
          'img'             : 'src',
          'image'           : 'xlink:href',
          'b-img'           : 'src',
          'b-img-lazy'      : ['src', 'blank-src'],
          'b-card'          : 'img-src',
          'b-card-img'      : 'img-src',
          'b-carousel-slide': 'img-src',
          'b-embed'         : 'src',
          'img-viewer'      : 'src',
        },
      },
    },
    plugins: [
      new webpack.ProvidePlugin({
        $     : 'jquery',
        jQuery: 'jquery',
      }),
    ],
  },
}
