FROM keymetrics/pm2:latest-stretch

ENV HOST="0.0.0.0"
ENV PORT=3020
ENV NPM_CONFIG_LOGLEVEL warn

WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app
COPY package-lock.json /usr/src/app
RUN npm ci

COPY . /usr/src/app

#RUN cp -n .env.example .env
RUN npm run lint
RUN npm run build

EXPOSE 3020

CMD [ "pm2-runtime", "start", "pm2.json", "--env", "production"]