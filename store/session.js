import {
  defaultMutations
} from 'vuex-easy-access'
import {
  filterMenu
} from '@/utils/menu'

const EXPIRED = 60 * 60 // 60 Minutes

export const state = () => {
  return {
    user: {
      email: '',
      free_portion: 0,
      level: '',
      remaining_free_portion: 0,
      role: '',
      user_id: '',
      user_name: '',
      username: '',
      karyawan: null,
    },
    options: [
      10,
      30,
      50,
    ],
    menus: [],
    token: null,
  }
}

export const mutations = {
  ...defaultMutations(state()),
  'token'(state, value) {
    state.token = value

    this.$axios.setToken(value, 'Bearer')
    this.$cookies.set('session/token', value, {
      path: '/',
      maxAge: EXPIRED,
    })
  },
  'setToken'(state, value) {
    state.token = value
    this.$axios.setToken(value, 'Bearer')
  }
}

export const getters = {
  role(state) {
    return state.user.role
  },
  menu(state, getters) {
    return filterMenu(getters.role, state.menus)
  },
  userId(state) {
    return state.user.user_id
  },
}

export const actions = {
  setInitToken({commit}){
    console.log(this.$cookies.get('session/token'))
    if (this.$cookies.get('session/token')){
      commit('setToken', this.$cookies.get('session/token'))
    }
  },
  init({
    dispatch
  }) {
    // return Promise.all([dispatch('load'), dispatch('getMenu')])
    return Promise.all([dispatch('load'), dispatch('getMenu')])
  },
  load({
    commit
  }) {
    return new Promise((resolve, reject) => {
      this.$axios.get('/user/me')
        .then((response) => {
          console.log("tekan kene suuuuuu", response)
          commit('user', response.data.data.user)
          resolve(response)
        })
        .catch(reject)
    })
  },
  getMenu({
    commit
  }) {
    return new Promise((resolve, reject) => {
      import('@/menu' /* webpackChunkName: "menu" */ )
        .then((menus) => {
          commit('menus', menus.default)
          resolve(menus.default)
        })
        .catch(reject)
    })
  },
  getId({
    state
  }) {
    return state.user.user_id
  },
  logout() {
    this.$cookies.remove('session/token')
  },
}
