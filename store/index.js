import EasyAccess, { defaultMutations } from 'vuex-easy-access'

export const state = () => {
  return { overlay: false }
}

export const mutations = { ...defaultMutations(state()) }

export const plugins = [ EasyAccess() ]

export const actions = {
  async nuxtServerInit ({ state, dispatch}){
    console.log('init reload')
    await dispatch('session/setInitToken')
  }
}
