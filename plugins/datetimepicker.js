import Vue from 'vue'
import VueTimePicker from 'vue2-timepicker'

Vue.use(VueTimePicker)
Vue.component('vue-timepicker', VueTimePicker)
