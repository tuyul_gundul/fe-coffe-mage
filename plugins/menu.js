export default async function (context) {
  const { app, store } = context
  const token          = app.$cookies.get('session/token')

  if (token) {
    try {
      await store.dispatch('session/init', context)
    } catch (err) {
      const code = parseInt(err.response && err.response.status)

      if (code !== 401)
        context.error(err)
    }
  }
}
