import _has from 'lodash/has'
import _set from 'lodash/set'

export default function ({
  $axios,
  app,
  redirect,
  route,
  store
}) {
  $axios.onRequest((config) => {
    if (route.name !== '/') {
      const token = app.$cookies.get('session/token')

      if (!_has(config.headers, 'Authorization') && token)
        _set(config.headers, 'Authorization', `Bearer ${token}`)
    }
  })

  $axios.onError((err) => {
    const code = parseInt(err.response && err.response.status)
    console.log(code)
    if (code === 401 && route.name !== '/')
      store.dispatch('session/logout')
    // app.$logout('/admin')

    // if (code === 403 && route.name !== '/')
    //   app.$logout('/403')
  })
}
